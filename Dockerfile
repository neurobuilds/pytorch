ARG BASE_DISTRO=ubuntu
ARG BASE_VERSION=20.04
ARG PYTHON_VERSION=3.10.4
ARG PYTORCH_VERSION=1.13.1
ARG CUDA_VERSION=11.8

FROM registry.gitlab.com/neurobuilds/miniconda:${PYTHON_VERSION}-${BASE_DISTRO}${BASE_VERSION} as final
LABEL com.nvidia.volumes.needed="nvidia_driver"

ARG PYTHON_VERSION
ARG PYTORCH_VERSION
ARG CUDA_VERSION

RUN /opt/conda/bin/conda install -c pytorch -c nvidia -y \
        pytorch=${PYTORCH_VERSION} \
        torchvision \
        pytorch-cuda=${CUDA_VERSION} && \
    /opt/conda/bin/conda clean -yaf
RUN echo -e "{\
    \n  \"python_version\": \"$PYTHON_VERSION\" \
    \n  \"cuda_version\": \"$CUDA_VERSION\" \
    \n  \"build_version\": \"$PYTORCH_VERSION\" \
    \n}" > /opt/conda/manifest.json

ENV PATH /opt/conda/bin:$PATH
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64
